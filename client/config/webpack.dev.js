var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-eval-source-map',

  output: {
    path: helpers.root('dist'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[id].chunk.js'
  },

  plugins: [
    new ExtractTextPlugin('[name].css'),
    new CopyWebpackPlugin([
      {
        from: helpers.root('src', 'assets', 'i18n'),
        to: 'assets/i18n'
      }
    ])
  ],

  devServer: {
    host: '0.0.0.0',
    historyApiFallback: true,
    stats: 'minimal',
    proxy: {
      '/api': {
        target: 'http://localhost:8888'
      }
    }
  }
});
