export interface List<T> {
  total: number
  result: T[]
}
