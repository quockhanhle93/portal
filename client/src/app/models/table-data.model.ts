import { TableRow } from './table.model'

export interface TableData {
  headers: string[]
  rows: TableRow[]
}
