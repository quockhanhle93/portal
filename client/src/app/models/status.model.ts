export interface Status {
  success: boolean
  message: string
  detail?: string
}
