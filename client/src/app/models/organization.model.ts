export interface Organization {
  _id: string
  name: string
  language: string
}
