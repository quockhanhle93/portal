export interface SelfService {
  type: string
  name: string
  description: string
  tags: string[]
}
