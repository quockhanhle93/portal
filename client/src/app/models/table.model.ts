export interface TableHeader extends Array<string> {}
export interface TableRow extends Array<string | number | boolean> {}
export interface TableRowSelection extends Object {}
