export interface IpamAddress {
  primary: string
  secondary: string
  tertiary: string
}
