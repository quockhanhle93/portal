export interface Location {
  address1: string
  address2: string
  city: string
  state: string
  country: string
  postal: number
}
