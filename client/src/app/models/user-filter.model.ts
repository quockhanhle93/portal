export interface UserFilter {
  organization?: string,
  role?: string,
  enabled?: string
}
