import { IpamAddress } from './ipam-address.model'

export interface OrganizationIpam {
  _id: string
  name: string
  version: string
  protocol: string
  port: number
  enabled: boolean
  dnsView: string
  environment: string
  ipAddress: IpamAddress
}
