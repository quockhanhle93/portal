export interface User {
  username: string,
  password: string,
  email: string,
  firstName: string,
  lastName: string,
  location: {
    address1: string,
    address2: string,
    city: string,
    state: string,
    country: string,
    postal: number
  },
  enabled: boolean,
  language: string,
  timezone: string,
  role: string,
  group: [Object],
  permission: [Object],
  orgs: [{
    _id: string,
    name: string,
    language: string,
  }]
  oldPassword: string,
  newPassword: string
}
