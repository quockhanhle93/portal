export interface ServerResponse<T> {
  data: T
  errors: object[]
}

