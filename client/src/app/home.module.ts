import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from './shared.module'
import { CoreModule } from './core.module'

import { WidgetRequestsComponent } from './components/widget-requests/widget-requests.component'
import { WidgetMotdComponent } from './components/widget-motd/widget-motd.component'
import { HomeComponent } from './components/home/home.component'

const routes: Routes = [
  { path: '', component: HomeComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    CoreModule
  ],
  declarations: [
    WidgetRequestsComponent,
    WidgetMotdComponent,
    HomeComponent
  ]
})
export class HomeModule {}
