import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from './shared.module'


import { PortalComponent } from './components/portal/portal.component'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthenticationInterceptor } from './interceptors/authentication.interceptor'

const routes: Routes = [
  { path: '', component: PortalComponent, children: [
    { path: 'home', loadChildren: './home.module#HomeModule' },
    { path: 'self-service', loadChildren: './self-service.module#SelfServiceModule' },
    { path: 'configuration', loadChildren: './configuration.module#ConfigurationModule' },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
  ]}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    PortalComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    }
  ]
})
export class PortalModule {}
