import { Injectable } from '@angular/core'
import { Organization } from '../models/organization.model'
import { OrganizationIpam } from '../models/organization-ipam.model'
import { HttpClient, HttpParams } from '@angular/common/http'
import { FormControl } from '@angular/forms'
import { Observable } from 'rxjs'

const ApiEndpoint = '/api/v1/organizations'

@Injectable()
export class OrganizationService {

  constructor(private http: HttpClient) {}

  fetch(): Observable<Organization[]> {
    return this.http.get<Organization[]>(ApiEndpoint)
  }

  get(organizationId: string): Observable<Organization> {
    return this.http.get<Organization>(ApiEndpoint + '/' + organizationId)
  }

  fetchIpams(organizationId: string): Observable<OrganizationIpam[]> {
    return this.http.get(ApiEndpoint + '/' + organizationId + '/ipams')
    .map(response => response as OrganizationIpam[] || null)
  }

  getIpam(organizationId: string, ipamId: string): Observable<OrganizationIpam> {
    return this.http.get(ApiEndpoint + '/' + organizationId + '/ipams/' + ipamId)
    .map(response => response as any || null)
  }

  addIpam(organizationId: string, ipam: OrganizationIpam): Observable<any> {
    return this.http.post(ApiEndpoint + '/' + organizationId + '/ipams', ipam)
    .map(response => response as any || null)
  }

  updateIpam(organizationId: string, ipamId: string, ipam: OrganizationIpam): Observable<any> {
    return this.http.put(ApiEndpoint + '/' + organizationId + '/ipams/' + ipamId, ipam)
    .map(response => response as any || null)
  }

  deleteIpam(organizationId: string, ipamId: string): Observable<any> {
    return this.http.delete(ApiEndpoint + '/' + organizationId + '/ipams/' + ipamId)
    .map(response => response as any || null)
  }

  getOrgs(id: string): Observable<Organization[]> {
    return this.http.get('/api/v1/organizations' + '/' + id).map(response => response as Organization[] || null)
  }

  update(organizationId: string, org: Organization): Observable<any> {
    return this.http.put(ApiEndpoint + '/' + organizationId, org)
      .map(response => response as any || null)
  }
}
