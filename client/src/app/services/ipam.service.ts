import { Injectable } from '@angular/core'
import { Ipam } from '../models/ipam.model'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

const ApiEndpoint = '/api/v1/ipams'

@Injectable()
export class IpamService {
  constructor(private http: HttpClient) {}

  fetch(): Observable<Ipam[]> {
    return this.http.get(ApiEndpoint).map(response => response as Ipam[] || null)
  }

  get(ipamId: string): Observable<Ipam> {
    return this.http.get(ApiEndpoint + '/' + ipamId).map(response => response as Ipam || null)
  }

  supportedIpamNames(): Observable<string[]> {
    return new Observable(observer => {
      let names = new Set<string>()
      this.fetch().subscribe(data => {
        if (data) {
          data.forEach(ipam => {
            names.add(ipam.name)
          })
        }
        observer.next(Array.from(names))
      })
    })
  }

  supportedIpamVersions(ipamName: string): Observable<string[]> {
    return new Observable(observer => {
      let versions = new Set<string>()
      this.fetch().subscribe(data => {
        if (data) {
          data.forEach(ipam => {
            if (ipam.name === ipamName) {
              versions.add(ipam.version)
            }
          })
        }
        observer.next(Array.from(versions))
      })
    })
  }
}
