import { Injectable } from '@angular/core'
import { ServerResponse } from '../models/server-response.model'
import { HttpClient } from '@angular/common/http'
import { User } from '../models/user.model'
import { Observable } from 'rxjs'
import 'rxjs/Rx'

const ApiEndpoint = '/api/v1/auth'

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<User> {
    return this.http.post(ApiEndpoint + '/login', {username, password}).map(response => response as User || null)
  }

  logout(): Observable<ServerResponse<any>> {
    return this.http.get(ApiEndpoint + '/logout').map(response => response as ServerResponse<any> || null)
  }
}
