import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {GlobalConfiguration} from '../models/global-configuration.model'
import {Language} from '../models/language.model'

const ApiEndpoint = '/api/v1/global'

@Injectable()
export class GlobalConfigurationService {

  constructor(private http: HttpClient) {
  }

  get(): Observable<GlobalConfiguration> {
    return this.http.get(ApiEndpoint).map(response => response as GlobalConfiguration || null)
  }

  getCurrentLanguage(): Observable<GlobalConfiguration> {
    return this.http.get(ApiEndpoint + '/languages?selected=true').map(response => response as GlobalConfiguration || null)
  }

  getLanguages(): Observable<Language[]> {
    return this.http.get(ApiEndpoint + '/languages').map(response => response as Language[] || null)
  }

  update(config: GlobalConfiguration): Observable<any> {
    return this.http.put(ApiEndpoint, config)
      .map(response => response as any || null)
  }
}
