import { EventEmitter, Injectable } from '@angular/core'
import { Route } from '../models/route.model'

@Injectable()
export class CoreService {
  public toggleSidebar: EventEmitter<any> = new EventEmitter()

  public mainRoutes: Route[] = [
    {
      label: 'Home',
      link: '/home'
    },
    {
      label: 'Self-Service',
      link: '/self-service'
    },
    {
      label: 'Configuration',
      link: '/configuration'
    }
  ]
}
