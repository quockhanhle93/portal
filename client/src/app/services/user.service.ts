import { Injectable } from '@angular/core'
import { List } from '../models/list.model'
import { User } from '../models/user.model'
import { UserFilter } from '../models/user-filter.model'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

const UserApiEndpoint = '/api/v1/users'

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  getUsers(page: number, limit: number, filter: UserFilter): Observable<List<User>> {
    let params = new HttpParams()
    params = params.append('page', page.toString())
    params = params.append('limit', limit.toString())
    if (filter.organization !== '-') {
      params = params.set('org', filter.organization)
    }
    if (filter.role !== '-') {
      params = params.set('role', filter.role)
    }
    if (filter.enabled !== '-') {
      params = params.set('enabled', filter.enabled)
    }
    return this.http.get<List<User>>(UserApiEndpoint, {
      params: params
    })
  }

  get(username: string): Observable<User> {
    return this.http.get(UserApiEndpoint + '/' + username).map(response => response as User || null)
  }

  delete(username: string): Observable<string> {
    return this.http.delete(UserApiEndpoint + '/' + username).map(response => response as string || null)
  }

  update(username: string, user: User): Observable<string> {
      return this.http.put(UserApiEndpoint + '/' + username, user).map(response => response as string || null)
  }

  addUser(user: User): Observable<any> {
      return this.http.post(UserApiEndpoint, user).map(response => response as any || null)
  }

  changePassword(username: string, user: User): Observable<any> {
      return this.http.put(UserApiEndpoint + '/' + username + '/password/reset', user).map(response => response as any || null)
  }

  resetPassword(username: string, user: User): Observable<any> {
      return this.http.put(UserApiEndpoint + '/' + username + '/password/reset', user).map(response => response as any || null)
  }

  changeStatus(username: string, enabled: boolean): Observable<string> {
    return this.http.put<string>(UserApiEndpoint + '/' + username + '/status', {
      enabled: enabled
    })
  }
}
