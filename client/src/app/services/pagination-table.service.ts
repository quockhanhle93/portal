import { Injectable } from '@angular/core'
import { TableRow, TableRowSelection } from '../models/table.model'

@Injectable()
export class PaginationTableService {

  public static getSelectedRows(rows: TableRow[], rowSelection: TableRowSelection): TableRow[] {
    let selectedRows: TableRow[] = []
    for (let property in rowSelection) {
      if (rowSelection.hasOwnProperty(property)) {
        if (rowSelection[property]) {
          selectedRows.push(rows[property])
        }
      }
    }
    return selectedRows
  }

  public static getPageCount(total: number, size: number) {
    return total % size === 0 ? total / size : Math.floor(total / size) + 1
  }

  public static getPageSizes() {
    return [ 10, 20, 50, 100 ]
  }
}
