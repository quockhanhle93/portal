import { Injectable } from '@angular/core'
import { SelfServiceGroup } from '../models/self-service-group.model'
import { SelfService } from '../models/self-service.model'

@Injectable()
export class SelfServiceService {

  public fetchSelfServiceGroups(): SelfServiceGroup[] {
    return [
      {
        name: 'All',
        query: 'all'
      },
      {
        name: 'Network',
        query: 'network'
      },
      {
        name: 'IP & DNS',
        query: 'ip-dns'
      },
      {
        name: 'DHCP',
        query: 'dhcp'
      },
      {
        name: 'UK Retail',
        query: 'uk-retail'
      },
      {
        name: 'Recent',
        query: 'recent'
      },
      {
        name: 'Favorite',
        query: 'favorite'
      }
    ]
  }

  public fetchSelfServices(): SelfService[] {
    return [
      {
        type: 'add',
        name: 'Add Block',
        description: 'Allows for the creation of one or more network containers.',
        tags: [ 'network' ]
      },
      {
        type: 'add',
        name: 'Add Network',
        description: 'Allows for the creation of one or more networks.\
          Networks can be marked as reserved which restricts their general usage,\
          but allows for futher notification of the network.',
        tags: [ 'network' ]
      },
      {
        type: 'modify',
        name: 'Modify Network',
        description: 'Provides the ability to modify networks in a reserved state.',
        tags: [ 'network' ]
      },
      {
        type: 'add',
        name: 'Add IPv4 Address and Host Record',
        description: 'Provides the ability to add a new IP address and Host Name (A Record).',
        tags: [ 'ip-dns' ]
      },
      {
        type: 'delete',
        name: 'Delete IPv4 Address  and Host Record',
        description: 'Provides the ability to modify networks in a reserved state.',
        tags: [ 'ip-dns' ]
      },
      {
        type: 'add',
        name: 'Add DNS Record',
        description: 'Allows for the addition of A, CNAME, and TXT records to an existing DNS.',
        tags: [ 'ip-dns' ]
      },
      {
        type: 'modify',
        name: 'Modify DNS Record',
        description: 'Allows for the modification of A, CNAME, and TXT records.',
        tags: [ 'ip-dns' ]
      },
      {
        type: 'other',
        name: 'Move DNS Record to New IP',
        description: 'Provides the ability to move DNS records associated with an IP address to a new IP address.\
          Limited to A, CNAM, and TXT records.',
        tags: [ 'ip-dns' ]
      },
      {
        type: 'add',
        name: 'Add DHCP Scope',
        description: 'Provides the ability to create a DHCP scope (commonly referred to as a range or pool) with inherited DHCP options.',
        tags: [ 'dhcp' ]
      },
      {
        type: 'other',
        name: 'Expand DHCP Scope Size',
        description: 'Provides the ability to extend an existing DHCP scope.',
        tags: [ 'dhcp' ]
      },
      {
        type: 'add',
        name: 'Add DHCP Reservation',
        description: 'Allows for the creation of a reserved (commonly referred to as a manual) DHCP address based on a unique MAC address.',
        tags: ['Network']
      },
      {
        type: 'other',
        name: 'Get Network List',
        description: 'Provides the ability to generate a csv file containing allocated networks.',
        tags: [ 'network' ]
      },
      {
        type: 'other',
        name: 'Get Store List',
        description: 'Provides the ability to generate a csv file containing the store domains.',
        tags: [ 'uk-retail' ]
      },
      {
        type: 'other',
        name: 'ISS Build Wincor',
        description: 'Provides the ability to provision ISS servers to the Wincor staging facility.',
        tags: [ 'uk-retail' ]
      },
      {
        type: 'other',
        name: 'Create, Modify, or Delete Store',
        description: 'Allows for the creation, modification, or deletion of a store.',
        tags: [ 'uk-retail' ]
      }
    ]
  }
}
