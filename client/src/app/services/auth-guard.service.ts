import {Injectable} from '@angular/core'
import {CanActivate, Router} from '@angular/router'
import * as Cookies from 'js-cookie'

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): boolean {
    const token = Cookies.get('authenticated')
    if (!token) {
      this.router.navigate(['login'])
      return false
    }
    return true
  }
}
