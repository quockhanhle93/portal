import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuardService } from './services/auth-guard.service'
import { AppComponent } from './components/app/app.component'
import { TranslateLoader, TranslateModule } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { GlobalConfigurationService } from './services/global-configuration.service'
import { ToastrModule } from 'ngx-toastr'

const routes: Routes = [
  { path: '', loadChildren: './portal.module#PortalModule', canActivate: [ AuthGuardService ] },
  { path: 'login', loadChildren: './login.module#LoginModule' },
  { path: '**', redirectTo: '', pathMatch: 'full' }
]

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http)
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-center',
      preventDuplicates: true,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    AuthGuardService,
    GlobalConfigurationService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
