import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'

import { RouterModule } from '@angular/router'

import { CoreService } from './services/core.service'
import { OrganizationService } from './services/organization.service'
import { IpamService } from './services/ipam.service'
import { SelfServiceService } from './services/self-service.service'
import { AuthService } from './services/auth.service'
import { UserService } from './services/user.service'

import { MainMenuComponent } from './components/main-menu/main-menu.component'
import { TopBarComponent } from './components/topbar/top-bar.component'
import { LogoComponent } from './components/logo/logo.component'
import { SearchComponent } from './components/search/search.component'
import { ButtonLogoutComponent } from './components/button-logout/button-logout.component'
import { ButtonMenuComponent } from './components/button-menu/button-menu.component'
import { MobileMenuComponent } from './components/mobile-menu/mobile-menu.component'
import { PortalFooterComponent } from './components/portal-footer/portal-footer.component'

import { AuthenticationInterceptor } from './interceptors/authentication.interceptor'
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    TranslateModule,
    FormsModule
  ],
  declarations: [
    MainMenuComponent,
    TopBarComponent,
    LogoComponent,
    SearchComponent,
    ButtonLogoutComponent,
    ButtonMenuComponent,
    MobileMenuComponent,
    PortalFooterComponent
  ],
  providers: [
    CoreService,
    OrganizationService,
    IpamService,
    SelfServiceService,
    AuthService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    }
  ],
  exports: [
    TopBarComponent,
    MobileMenuComponent,
    PortalFooterComponent,
    TranslateModule
  ]
})
export class SharedModule {}
