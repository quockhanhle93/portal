import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from './shared.module'
import { ConfigurationComponent } from './components/configuration/configuration.component'
import { MyAccountComponent } from './components/my-account/my-account.component'
import { GlobalConfigurationComponent } from './components/global-configuration/global-configuration.component'
import { GlobalConfigurationService } from './services/global-configuration.service'
import { CoreModule } from './core.module'

const routes: Routes = [
  { path: '', component: ConfigurationComponent, children: [
    { path: '', redirectTo: 'my-account', pathMatch: 'full' },
    { path: 'my-account', component: MyAccountComponent },
    { path: 'global', component: GlobalConfigurationComponent },
    { path: 'organizations', loadChildren: './ipam-configuration.module#IpamConfigurationModule' },
    { path: 'users', loadChildren: './user-configuration.module#UserConfigurationModule' }
  ]}
]

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CoreModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ConfigurationComponent,
    GlobalConfigurationComponent,
    MyAccountComponent
  ],
  providers: [
    GlobalConfigurationService
  ]
})
export class ConfigurationModule {}
