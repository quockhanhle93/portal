import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from '../../services/auth.service'
import { AuthGuardService } from '../../services/auth-guard.service'
import { TranslateService } from '@ngx-translate/core'
import { OrganizationService } from '../../services/organization.service'
import { GlobalConfigurationService } from '../../services/global-configuration.service'

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  submiting: boolean
  hasError: boolean
  username: ''
  password: ''

  constructor(private router: Router, private authService: AuthService, private authGuardService: AuthGuardService,
              private translate: TranslateService, private organizationService: OrganizationService,
              private globalConfigService: GlobalConfigurationService) {
  }

  ngOnInit(): void {
    if (this.authGuardService.canActivate()) {
      this.router.navigate(['home'])
    } else {
      this.globalConfigService.getCurrentLanguage().subscribe(response => {
        this.translate.use(response.language)
      })
    }
  }

  login() {
    this.submiting = true
    this.authService.login(this.username, this.password).subscribe(response => {
      this.submiting = false
      if (response) {
        if (response.language) {
          this.translate.use(response.language)
          localStorage.setItem('language', response.language)
        } else {
          this.organizationService.get(response.orgs[0]._id).subscribe(organization => {
            if (organization && organization.language) {
              this.translate.use(organization.language)
              localStorage.setItem('language', organization.language)
            }
          })
        }
        this.router.navigate(['home'])
      }
    }, er => {
      this.hasError = true
      this.submiting = false
    })
  }
}
