import { Component, OnInit, OnDestroy } from '@angular/core'
import { SelfService } from '../../models/self-service.model'
import { SelfServiceGroup } from '../../models/self-service-group.model'
import { SelfServiceService } from '../../services/self-service.service'
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'self-service',
  templateUrl: './self-service.component.html',
  styleUrls: ['./self-service.component.css']
})
export class SelfServiceComponent implements OnInit, OnDestroy {
  private routeQueryParamsSubscription: any

  private filter: string

  private selfServiceGroups: SelfServiceGroup[]
  private selfServices: SelfService[]

  constructor(private selfServiceService: SelfServiceService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.selfServiceGroups = this.selfServiceService.fetchSelfServiceGroups()
    this.selfServices = this.selfServiceService.fetchSelfServices()
    this.routeQueryParamsSubscription = this.route.queryParams.subscribe(params => {
      this.filter = params['filter']
      if (!this.filter) {
        // append '?filter=all' on default router path
        this.router.navigate(['/self-service'], {
          queryParams: { 'filter': 'all' }
        })
      }
    })
  }

  ngOnDestroy() {
    this.routeQueryParamsSubscription.unsubscribe()
  }

  filterSelfServices(filterValue: string) {
    if (filterValue === 'all') {
      return this.selfServices
    }
    if (filterValue === 'recent') {
      return [{
        name: 'Not Implemented Yet',
        description: 'A sample recent service.'
      }]
    }
    return this.selfServices.filter((item) => {
      return item.tags.indexOf(filterValue) > -1
    })
  }

  getColorForServiceType(type: string) {
    if (type === 'add') {
      return 'green'
    }
    if (type === 'modify') {
      return 'blue'
    }
    if (type === 'delete') {
      return 'red'
    }
    return 'purple'
  }
}
