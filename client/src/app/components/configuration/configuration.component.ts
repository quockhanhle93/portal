import { Component } from '@angular/core'

@Component({
  selector: 'configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent {

  groups = [
    {
      name: 'My Account',
      link: 'my-account'
    },
    {
      name: 'Organizations',
      link: 'organizations'
    },
    {
      name: 'Users',
      link: 'users'
    },
    {
      name: 'Global',
      link: 'global'
    }
  ]
}
