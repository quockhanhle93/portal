import { Component, Input, Output, OnChanges, SimpleChanges, OnInit, EventEmitter } from '@angular/core'

@Component({
  selector: 'core-select-spinner',
  templateUrl: './core-select-spinner.component.html',
  styleUrls: ['./core-select-spinner.component.css']
})
export class CoreSelectSpinnerComponent implements OnChanges, OnInit {
  @Input('min') min: number = null
  @Input('max') max: number = null
  @Input('disabled') disabled = false
  @Input() value: number = null
  @Output() valueChange = new EventEmitter<number>()

  private values = this.generateValues(this.min, this.max)

  private generateValues(min: number, max: number): Array<number> {
    let values = []
    for (let i = min, n = max; i <= n; i++) {
      values.push(i)
    }
    return values
  }

  private onValueChange(value: string): void {
    this.value = +value
    this.fireValueChange()
  }

  private increaseValue() {
    if (this.value < this.max) {
      this.value++
      this.fireValueChange()
    }
  }

  private decreaseValue() {
    if (this.value > this.min) {
      this.value--
      this.fireValueChange()
    }
  }

  private fireValueChange() {
    this.valueChange.emit(this.value)
  }

  ngOnInit() {
    if (!this.disabled) {
      if (this.min === null && !Number.isInteger(this.min)) {
        throw new Error('[min] (integer) is required')
      }
      if (!this.max === null && !Number.isInteger(this.max)) {
        throw new Error('[max] (integer) is required')
      }
      if (!this.value === null && !Number.isInteger(this.value)) {
        throw new Error('[value] (integer) is required')
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.disabled) {
      if (this.min > this.max) {
        throw new Error('[min=' + this.min + '] must be not greater than [max=' + this.max + ']')
      }
      if (this.value < this.min) {
        throw new Error('[value=' + this.value + '] must be greater than or equal to [min=' + this.min + ']')
      }
      if (this.value > this.max) {
        throw new Error('[value=' + this.value + '] must be less than or equal to [max=' + this.max + ']')
      }
      if ((changes.min && changes.min.previousValue !== changes.min.currentValue)
      || (changes.max && changes.max.previousValue !== changes.max.currentValue)) {
        this.values = this.generateValues(this.min, this.max)
      }
    }
  }
}
