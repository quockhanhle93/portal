import { Component } from '@angular/core'

import '../../../assets/css/styles.css'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en')
    if (localStorage.getItem('language')) {
      translate.use(localStorage.getItem('language'))
    }
  }
}
