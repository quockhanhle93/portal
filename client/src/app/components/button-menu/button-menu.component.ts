import { Component, ElementRef, HostListener } from '@angular/core'
import { CoreService } from '../../services/core.service'

@Component({
  selector: 'button-menu',
  templateUrl: './button-menu.component.html',
  styleUrls: ['./button-menu.component.css']
})
export class ButtonMenuComponent {
  constructor(private coreService: CoreService, private hostElement: ElementRef) {
    this.coreService.toggleSidebar.subscribe(() => {
      console.log('should update menu icon ?')
    })
  }
  @HostListener('click') onClick() {
    this.coreService.toggleSidebar.emit()
  }
}
