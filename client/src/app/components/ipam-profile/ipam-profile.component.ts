import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'
import { Organization } from '../../models/organization.model'
import { IpamAddress } from '../../models/ipam-address.model'
import { OrganizationIpam } from '../../models/organization-ipam.model'
import { OrganizationService } from '../../services/organization.service'
import { IpamService } from '../../services/ipam.service'
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'

interface Status {
  success: boolean
  message: string
}

@Component({
  selector: 'ipam-profile',
  templateUrl: './ipam-profile.component.html',
  styleUrls: ['./ipam-profile.component.css']
})
export class IpamProfileComponent implements OnInit, OnDestroy {
  organization = <Organization>{}
  ipam = <OrganizationIpam> {
    protocol: 'HTTP',
    port: 80,
    ipAddress: {}
  }
  mode = 'new'
  title = ''
  status = <Status> {}
  routerEventSub: any
  myForm: FormGroup
  supportedIpamNames: string[] = []
  supportedIpamVersions: string[] = []

  constructor(private route: ActivatedRoute,
    private organizationService: OrganizationService,
    private ipamService: IpamService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) {}

  ngOnInit() {
    this.buildForm()
    this.ipamService.supportedIpamNames().subscribe(names => {
      this.supportedIpamNames = names
    })
    this.checkAddingOrUpdating()
    this.routerEventSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.checkAddingOrUpdating()
      }
    })
  }

  ngOnDestroy() {
    this.routerEventSub.unsubscribe()
  }

  private onIpamNameChange(ipamName: string) {
    if (ipamName) {
      this.ipamService.supportedIpamVersions(ipamName).subscribe(versions => {
        this.supportedIpamVersions = versions
      })
    }
  }

  private buildForm(): void {
    this.myForm = this.formBuilder.group({
      'ipamName': ['', [Validators.required]],
      'ipamProtocol': ['', [Validators.required]],
      'ipamPort': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'ipamVersion': ['', [Validators.required]],
      'ipamPrimaryIp': ['', [Validators.required]],
      'ipamSecondaryIp': ['', []],
      'ipamTertiaryIp': ['', []],
      'ipamDnsView': ['', [Validators.required]],
      'ipamEnvironment': ['', [Validators.required]]
    })
  }

  private checkAddingOrUpdating() {
    let action = this.route.snapshot.params['action']
    if (action === 'new') {
      this.prepareIpamProfileToAdd()
    } else if (action === 'update') {
      this.loadIpamProfileToUpdate()
    }
  }

  private prepareIpamProfileToAdd() {
    this.mode = 'new'
    this.title = 'New DDI Endpoint'
    this.route.queryParams.subscribe(params => {
      let organizationId = params['organization']
      if (organizationId) {
        this.organizationService.get(organizationId).subscribe(data => {
          if (data) {
            this.organization = data
          }
        }, error => {
          this.toastr.error(error.message)
        })
      }
    }).unsubscribe()
  }

  private loadIpamProfileToUpdate() {
    this.mode = 'update'
    this.title = 'Update DDI Endpoint'
    this.route.queryParams.subscribe(params => {
      const organizationId = params['organization']
      const ipamId = params['ipam']
      if (organizationId && ipamId) {
        this.organizationService.get(organizationId).subscribe(response => {
          if (response) {
            this.organization = response
            this.organizationService.getIpam(this.organization._id, ipamId).subscribe(response1 => {
              if (response1) {
                let ipam = response1
                if (!ipam.ipAddress) {
                  ipam.ipAddress = <IpamAddress> {}
                }
                this.ipam = ipam
              }
            })
          }
        })
      }
    }).unsubscribe()
  }

  private saveNew(): void {
    this.organizationService.addIpam(this.organization._id, this.ipam).subscribe(response => {
      if (response) {
        this.router.navigate(['configuration/organizations/update'], {
          queryParams: {
            organization: this.organization._id,
            ipam: response.id
          }
        })
        this.toastr.success('Saved Successfully')
      } else {
        this.toastr.error('Failed to add IPAM')
      }
    }, error => {
      this.toastr.error(error.message)
    })
  }

  private update(): void {
    this.organizationService.updateIpam(this.organization._id, this.ipam._id, this.ipam).subscribe(response => {
      if (response) {
        this.toastr.success('Updated Successfully')
      } else {
        this.toastr.error('Failed to update')
      }
    }, error => {
      this.toastr.error(error.message)
    })
  }

  private delete(): void {
    if (confirm('Are you sure you want to delete this DDI Endpoint?')) {
      this.organizationService.deleteIpam(this.organization._id, this.ipam._id).subscribe(response => {
        this.router.navigate(['configuration/organizations'])
      })
    }
  }
}
