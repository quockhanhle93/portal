import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core'

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements AfterViewInit {
  showingSearchInput = false

  @ViewChild('searchInput') searchInput: ElementRef

  ngAfterViewInit() {}

  startSearch() {
    this.showingSearchInput = true
    setTimeout(() => {
      this.searchInput.nativeElement.focus()
    }, 0)
  }

  stopSearch() {
    this.showingSearchInput = false
    this.searchInput.nativeElement.value = ''
  }

  onKeyDown(e: KeyboardEvent) {
    if (e.keyCode === 27) {
      this.stopSearch()
    }
  }
}
