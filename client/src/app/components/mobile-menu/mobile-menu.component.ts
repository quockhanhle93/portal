import { Component, ElementRef } from '@angular/core'
import { CoreService } from '../../services/core.service'
import { Route } from '../../models/route.model'

@Component({
  selector: 'mobile-menu',
  templateUrl: './mobile-menu.component.html',
  styleUrls: ['./mobile-menu.component.css']
})
export class MobileMenuComponent {
  private routes: Route[]

  constructor(private coreService: CoreService, private hostElement: ElementRef) {
    this.routes = this.coreService.mainRoutes
    this.coreService.toggleSidebar.subscribe(() => {
      if (hostElement.nativeElement.classList.contains('invisible')) {
        hostElement.nativeElement.classList.remove('invisible')
      } else {
        hostElement.nativeElement.classList.add('invisible')
      }
    })
  }

  closeMenu() {
    this.coreService.toggleSidebar.emit()
  }
}
