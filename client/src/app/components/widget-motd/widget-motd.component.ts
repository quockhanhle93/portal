import { Component } from '@angular/core'

@Component({
  selector: 'widget-motd',
  templateUrl: './widget-motd.component.html',
  styleUrls: ['./widget-motd.component.css']
})
export class WidgetMotdComponent {}
