import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'
import { User } from '../../models/user.model'
import { UserService } from '../../services/user.service'
import { OrganizationService } from '../../services/organization.service'
import { FormGroup, Validators, FormBuilder, EmailValidator, ValidatorFn, AbstractControl } from '@angular/forms'
import { Organization } from '../../models/organization.model'
import { Status } from '../../models/status.model'
import { fade } from '../../animations/fade.animation'
import { Language } from '../../models/language.model'
import { GlobalConfigurationService } from '../../services/global-configuration.service'
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  animations: [
    fade('animation')
  ]
})
export class UserProfileComponent implements OnInit {
  user = <User> {
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    email: '',
    location: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      country: '',
      postal: 0
    },
    enabled: true,
    language: 'en',
    timezone: '',
    role: 'user',
    group: [],
    permission: [],
    orgs: []
  }
  password = ''
  title = ''
  mode = 'new'
  status = <Status> {}
  roles = ['user', 'normal', 'master']
  routerEventSub: any
  myForm: FormGroup
  orgs: Organization[]
  languages: Language[]
  processing = false

  constructor(private route: ActivatedRoute,
              private orgService: OrganizationService,
              private userService: UserService,
              private router: Router,
              private formBuilder: FormBuilder,
              private globalConfigService: GlobalConfigurationService,
              private toastr: ToastrService) {}

  ngOnInit() {
    this.buildForm()
    this.checkAction()
    this.routerEventSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.checkAction()
      }
    })
  }

  private buildForm(): void {
    this.myForm = this.formBuilder.group({
      'username': ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]+$')]],
      'password': ['', []],
      'email': ['', [Validators.required, Validators.email]],
      'firstName': ['', []],
      'lastName': ['', []],
      'address1': ['', []],
      'address2': ['', []],
      'city': ['', []],
      'state': ['', []],
      'country': ['', []],
      'postal': ['', []],
      'language': ['', []],
      'timezone': ['', []],
      'role': ['', []],
      'orgs': ['', []],
      'newPassword': ['', []],
    })
    let passwordControl = this.myForm.get('password')
    passwordControl.setValidators([Validators.required, Validators.minLength(6)])
    passwordControl.updateValueAndValidity()
  }

  private checkAction() {
    this.orgService.fetch().subscribe(data => {
    this.orgs = data
    if (this.orgs && this.orgs[0]) {
        this.user.orgs[0] = this.orgs[0]
      }
    }, error => {
      this.toastr.error(error.message)
    })
    this.globalConfigService.getLanguages().subscribe(data => {
      this.languages = data
    }, error => {
      this.toastr.error('Can not get supported languages')
    })
    let action = this.route.snapshot.params['action']
    if (action === 'new') {
      this.prepareUserProfileToAdd()
    } else if (action === 'update') {
      this.loadUserProfileToUpdate()
    }
  }

  private loadUserProfileToUpdate() {
    this.mode = 'update'
    this.title = 'Update User'
    this.processing = true
    this.myForm.get('password').disable()
    this.route.queryParams.subscribe(params => {
      let username = params['username']
      this.userService.get(username).subscribe(response => {
        let user = response
        this.user = user
        if (this.user.orgs.length === 0) {
          this.user.orgs = [<Organization> {
            _id: '',
            name: '',
            language: ''
          }]
        }
        this.processing = false
      })
    }).unsubscribe()
  }

  private prepareUserProfileToAdd() {
    this.mode = 'new'
    this.title = 'New User'
    this.myForm.get('password').enable()
  }

  private prepareToResetPassword() {
    this.mode = 'reset-password'
    this.title = 'Update Password'
    let newPasswordControl = this.myForm.get('newPassword')

    newPasswordControl.setValidators([Validators.required, Validators.minLength(6)])
    newPasswordControl.updateValueAndValidity()
  }

  private saveNew(): void {
    if (this.user.orgs[0]._id === '') {
      this.user.orgs.pop()
    }
    this.processing = true
    this.userService.addUser(this.user).subscribe(response => {
      if (response) {
        this.router.navigate(['configuration/users/update'], {
          queryParams: {
            username: response.username
          }
        })
        this.loadUserProfileToUpdate()
        this.toastr.success('Saved Successfully')
      }
      this.processing = false
    }, error => {
      if (error.status === 409) {
        this.toastr.error('Username or Email is duplicated')
      } else {
        this.toastr.error('Cannot save user')
      }
      this.checkAction()
      this.processing = false
    })
  }

  private update(username: string): void {
    this.processing = true
    let prepareData = new Promise((resolve) => {
      if (this.user.orgs[0]._id === '') {
        this.user.orgs.pop()
        resolve(this.user)
      } else {
        this.orgService.get(this.user.orgs[0]._id).subscribe(data => {
          this.user.orgs = [data]
          resolve(this.user)
        })
      }
    })
    prepareData.then((data) => {
      this.userService.update(username, <User>data).subscribe(user => {
        if (user) {
          this.toastr.success('Updated Successfully')
        }
        this.loadUserProfileToUpdate()
        this.processing = false
      }, error => {
        if (error.status === 409) {
          this.toastr.error('Username or Email is duplicated')
        } else {
          this.toastr.error('Cannot update user')
        }
        this.loadUserProfileToUpdate()
        this.processing = false
      })
    })
  }

  private delete(username: string): void {
    if (confirm('Are you sure you want to delete this User ?')) {
      this.processing = true
      this.userService.delete(username).subscribe(response => {
        this.processing = false
        this.router.navigate(['configuration/users'])
      }, error => {
        if (error.status === 404) {
          this.toastr.error('Cannot delete user')
        } else {
          this.toastr.error('Something went wrong')
        }
        this.processing = false
      })
    }
  }

  private resetPassword(username: string): void {
    this.processing = true
    this.user.password = this.user.newPassword
    this.userService.resetPassword(username, this.user).subscribe(response => {
      if (response) {
        this.router.navigate(['configuration/users/update'], {
          queryParams: {
            username: response.username
          }
        })
        this.loadUserProfileToUpdate()
        this.toastr.success('Password has been reset successfully')
      }
      this.processing = false
    }, (error) => {
      this.toastr.error('Cannot reset password')
      this.processing = false
    })
  }

  private routeToResetPassword(username: string): void {
    this.prepareToResetPassword()
  }

  ngOnDestroy() {
    this.routerEventSub.unsubscribe()
  }
}
