import { Component } from '@angular/core'
import { Route } from '../../models/route.model'
import { CoreService } from '../../services/core.service'

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent {
  private routes: Route[]

  constructor(private coreService: CoreService) {
    this.routes = this.coreService.mainRoutes
  }
}
