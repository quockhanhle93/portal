import { Component } from '@angular/core'

@Component({
  selector: 'portal-footer',
  templateUrl: './portal-footer.component.html',
  styleUrls: ['./portal-footer.component.css']
})
export class PortalFooterComponent {}
