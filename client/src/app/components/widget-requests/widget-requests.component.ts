import { Component } from '@angular/core'
import {TranslateService} from '@ngx-translate/core'

@Component({
  selector: 'widget-requests',
  templateUrl: './widget-requests.component.html',
  styleUrls: ['./widget-requests.component.css']
})
export class WidgetRequestsComponent {
  public searchString: string
  public statusFilterString: string
  public requestTableHeaders: string[]
  public requestStatuses: string[]

  public requestTableRows = [
    [
      1,
      'Self-Service',
      'Create, Modify or Delete Store 9911',
      'Open',
      '6/14/2017',
      '',
      '',
      ''
    ],
    [
      2,
      'Self-Service',
      'Add DNS Entries Error',
      'Open',
      '6/14/2017',
      '',
      '',
      ''
    ],
    [
      3,
      'Self-Service',
      'Create Store 9964',
      'Open',
      '6/14/2017',
      '',
      '',
      ''
    ],
    [
      4,
      'Self-Service',
      'Get Store List',
      'Completed',
      '6/14/2017',
      '',
      '',
      ''
    ]
  ]

  constructor(private translate: TranslateService) {
    this.requestTableHeaders = [
      this.translate.instant('ID'),
      this.translate.instant('Category'),
      this.translate.instant('Type'),
      this.translate.instant('Status'),
      this.translate.instant('Start'),
      this.translate.instant('End'),
      this.translate.instant('MACD Qty'),
      this.translate.instant('Comment')
    ]
    this.requestStatuses = [
      this.translate.instant('All'),
      this.translate.instant('Open'),
      this.translate.instant('Completed')
    ]
  }
}
