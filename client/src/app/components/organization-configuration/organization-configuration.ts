import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { TableData } from '../../models/table-data.model'
import { Organization } from '../../models/organization.model'
import { OrganizationService } from '../../services/organization.service'
import {ToastrService} from 'ngx-toastr'
import {GlobalConfigurationService} from '../../services/global-configuration.service'
import {Language} from '../../models/language.model'
import { fade } from '../../animations/fade.animation'

@Component({
  selector: 'ipam-list',
  templateUrl: './organization-configuration.html',
  styleUrls: ['./organization-configuration.css'],
  animations: [
    fade('animation')
  ]
})
export class OrganizationConfigurationComponent implements OnInit {
  private tableData: TableData = {
    headers: [ 'ID', 'DDI Endpoint', 'Version', 'Protocol', 'Port', 'Environment', 'DNS View', 'Status' ],
    rows: []
  }
  private organizations: Organization[]
  private selectedOrganizationId: string
  private selectedOrganization: Organization
  private allowNewIpam = false
  private languages: Language[]
  private updatingProfile: boolean
  private processing: boolean

  constructor(private organizationService: OrganizationService, private router: Router, private toastr: ToastrService,
              private globalService: GlobalConfigurationService) {}

  ngOnInit() {
    this.processing = true
    this.organizationService.fetch().subscribe(data => {
      if (data) {
        this.processing = false
        this.organizations = data
        if (this.organizations.length > 0) {
          this.selectedOrganizationId = this.organizations[0]._id
          this.populateIpams(this.selectedOrganizationId)
          this.populateProfile(this.selectedOrganizationId)
        }
      }
    })
    this.globalService.getLanguages().subscribe(response => {
      this.languages = response
    }, err => {
      this.toastr.error('Can not get supported languages', 'Organization configuration')
    })
  }

  private onOrganizationChange(event: Event): void {
    this.populateIpams(event.toString())
    this.populateProfile(event.toString())
  }

  private routeToNewIpam(): void {
    this.router.navigate(['/configuration/organizations/new'], {
      queryParams: {
        organization: this.selectedOrganizationId
      }
    })
  }

  private routeToUpdateIpam(ipamId: string): void {
    this.router.navigate(['/configuration/organizations/update'], {
      queryParams: {
        organization: this.selectedOrganizationId,
        ipam: ipamId
      }
    })
  }

  private populateIpams(organizationId: string): void {
    this.organizationService.fetchIpams(organizationId).subscribe(response => {
      if (response) {
        this.tableData.rows = []
        for (let i = 0, n = response.length; i < n; i++) {
          let ipam = response[i]
          let enabled = ipam.enabled ? 'Active' : ''
          this.tableData.rows.push([ ipam._id, ipam.name, ipam.version, ipam.protocol, ipam.port, ipam.environment, ipam.dnsView, enabled ])
        }
        this.allowNewIpam = this.tableData.rows.length === 0 ? true : false
      }
    })
  }

  private populateProfile(organizationId: string): void {
    this.selectedOrganization = {...this.organizations.filter(org => org._id === organizationId)[0]}
  }

  private updateProfile(): void {
    this.updatingProfile = true
    this.organizationService.update(this.selectedOrganization._id, this.selectedOrganization).subscribe(response => {
      let org = this.organizations.filter(org2 => org2._id === this.selectedOrganization._id)[0]
      org.name = this.selectedOrganization.name
      org.language = this.selectedOrganization.language
      this.updatingProfile = false
      this.toastr.success('Saved Successfully', 'Organization configuration')
    }, err => {
      this.updatingProfile = false
      this.toastr.error('Something went wrong', 'Organization configuration')
    })
  }
}
