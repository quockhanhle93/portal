import { Component, HostListener } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from '../../services/auth.service'
import * as Cookies from 'js-cookie'

@Component({
  selector: 'button-logout',
  templateUrl: './button-logout.component.html',
  styleUrls: ['./button-logout.component.css']
})
export class ButtonLogoutComponent {
  constructor(private router: Router, private authService: AuthService) {}

  @HostListener('click') onClick() {
    this.logout()
  }

  logout() {
    this.authService.logout().subscribe(response => {
      Cookies.remove('authenticated')
      this.router.navigate(['login'])
    })
  }
}
