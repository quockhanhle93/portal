import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { User } from '../../models/user.model'
import { UserFilter } from '../../models/user-filter.model'
import { UserService } from '../../services/user.service'
import { OrganizationService } from '../../services/organization.service'
import { Organization } from '../../models/organization.model'
import { PaginationTableService } from '../../services/pagination-table.service'
import { TableRow, TableHeader, TableRowSelection } from '../../models/table.model'
import { fade } from '../../animations/fade.animation'
import { ToastrService } from 'ngx-toastr'

const USER_ENABLED = 'Enabled'
const USER_DISABLED = 'Disabled'

interface Filter {
  name: string
  value: string
}

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  animations: [
    fade('animation')
  ]
})
export class UserListComponent implements OnInit {

  private headers: TableHeader = ['Username', 'First Name', 'Last Name', 'Email', 'Role', 'Associated Organization', 'Status']
  private rows: TableRow[] = []
  private rowSelection: TableRowSelection = {}
  private pageSizes = PaginationTableService.getPageSizes()
  private selectedPageSize = this.pageSizes[0]
  private selectedPage = 1
  private pageCount: number
  private processing = false
  private searching = ''
  private hasUserSelected = false

  private organizationFilters: Filter[] = []

  private roleFilters = [{
    name: 'All',
    value: '-'
  }, {
    name: 'Master Administrator',
    value: 'master'
  }, {
    name: 'Normal Administrator',
    value: 'normal'
  }, {
    name: 'User',
    value: 'user'
  }]

  private statusFilters = [{
    name: 'All',
    value: '-'
  }, {
    name: USER_ENABLED,
    value: 'true'
  }, {
    name: USER_DISABLED,
    value: 'false'
  }]

  private selectedFilter: UserFilter = {
    organization: '-',
    role: '-',
    enabled: '-'
  }

  constructor(private userService: UserService, private router: Router, private toastr: ToastrService,
    private organizationService: OrganizationService) {}

  ngOnInit() {
    this.populateUsers()
  }

  private deleteUser(username: string) {
    const selectedRows = PaginationTableService.getSelectedRows(this.rows, this.rowSelection)
    for (let i = 0; i < selectedRows.length; i++) {
      this.userService.delete(selectedRows[i][0].toString()).subscribe(data => {
        if (data) {
          this.toastr.success('User(s) deleted successfully')
        }
        this.rowSelection = {}
      }, error => {
        if (error.status === 404) {
          this.toastr.error('Failed to delete user(s)')
        } else {
          this.toastr.error(error.message)
        }
      })
    }
    this.populateUsers()
  }

  private populateUsers(): void {
    this.processing = true
    this.organizationFilters = [{
      name: 'All',
      value: '-'
    }]
    this.organizationService.fetch().subscribe(organizations => {
      if (organizations) {
        for (let i = 0, n = organizations.length; i < n; i++) {
          this.organizationFilters.push({
            name: organizations[i].name,
            value: organizations[i].name
          })
        }
        this.userService.getUsers(this.selectedPage, this.selectedPageSize, this.selectedFilter).subscribe(data => {
          if (data) {
            const totalUserCount = data.total
            this.rows = []
            if (totalUserCount > 0) {
              const users: User[] = data.result
              for (let i = 0; i < users.length; i++) {
                const user = users[i]
                const enabled = user.enabled ? USER_ENABLED : USER_DISABLED
                const organization = user.orgs && user.orgs[0] && user.orgs[0].name ? user.orgs[0].name : ''
                const row: TableRow = [ user.username, user.firstName, user.lastName, user.email, user.role, organization, enabled ]
                this.rows.push(row)
              }
              this.pageCount = PaginationTableService.getPageCount(totalUserCount, this.selectedPageSize)
            }
          }
          this.processing = false
        }, error => {
          if (error.status === 504) {
            this.toastr.error('Please check your internet connection!')
          } else {
            this.toastr.error(error.message)
          }
        })
      }
    }, organizationError => {
      if (organizationError.status === 504) {
        this.toastr.error('Please check your internet connection!')
      } else {
        this.toastr.error(organizationError.message)
      }
    })
  }

  private navigateNewUserPage(): void {
    this.router.navigate(['/configuration/users/new'])
  }

  private navigateUpdateUserPage(selectedCellValues: Array<string>, selectedCellValue: string, selectedCellIndex: number): void {
    let username = selectedCellValues[0]
    this.router.navigate(['/configuration/users/update'], {
      queryParams: {
        username: username
      }
    })
  }

  private getColumnRenders() {
    return ['', '', '', '', '', '', this.getCssClassForStatusColumn]
  }

  private getCssClassForStatusColumn(cellValue: string) {
    return cellValue === USER_ENABLED ? 'special-cell green' : 'special-cell red'
  }

  private onRowSelectionChange(rowSelection: Object): void {
    const selectedRows = PaginationTableService.getSelectedRows(this.rows, this.rowSelection)
    this.hasUserSelected = selectedRows.length > 0 ? true : false
  }

  private onPageChange(page: number): void {
    this.rowSelection = {}
    this.populateUsers()
  }

  private onItemsPerPageChange(itemsPerPage: number): void {
    this.rowSelection = {}
    this.selectedPage = 1
    this.populateUsers()
  }

  private onFilterChange() {
    this.rowSelection = {}
    this.populateUsers()
  }

  private changeStatus(): void {
    const selectedRows = PaginationTableService.getSelectedRows(this.rows, this.rowSelection)
    for (let i = 0; i < selectedRows.length; i++) {
      const selectedUsername = selectedRows[i][this.headers.indexOf('Username')].toString()
      const selectedStatus = selectedRows[i][this.headers.indexOf('Status')].toString()
      this.userService.changeStatus(selectedUsername, selectedStatus === USER_ENABLED ? false : true).subscribe(data => {
        if (data) {
          this.toastr.success('Status(es) changed successfully')
        }
      }, error => {
        if (error.status === 404) {
          this.toastr.error('Failed to change status(es)')
        } else {
          this.toastr.error(error.message)
        }
      })
    }
    this.populateUsers()
  }
}
