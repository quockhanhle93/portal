import {Component, OnInit} from '@angular/core'
import {GlobalConfigurationService} from '../../services/global-configuration.service'
import {GlobalConfiguration} from '../../models/global-configuration.model'
import {ToastrService} from 'ngx-toastr'
import {Language} from '../../models/language.model'
import { fade } from '../../animations/fade.animation'

@Component({
  selector: 'global-configuration',
  templateUrl: './global-configuration.component.html',
  animations: [
    fade('animation')
  ]
})
export class GlobalConfigurationComponent implements OnInit {
  config: GlobalConfiguration
  languages: Language[]
  processing: boolean

  constructor(private globalService: GlobalConfigurationService, private toastr: ToastrService) {
    this.processing = true
    globalService.getLanguages().subscribe(response => {
      this.languages = response
      this.processing = false
    }, err => {
      this.processing = false
      this.toastr.error('Can not get supported languages')
    })
  }

  ngOnInit(): void {
    this.globalService.get().subscribe(response =>
      this.config = response[0])
  }

  private update() {
    this.globalService.update(this.config).subscribe(response => {
      this.toastr.success('Saved Successfully')
    }, err => {
      this.toastr.error('Something went wrong', 'Error')
    })
  }
}
