import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { CoreTableComponent} from './components/core-table/core-table.component'
import { CoreSelectSpinnerComponent} from './components/core-select-spinner/core-select-spinner.component'
import { CoreLoaderComponent } from './components/core-loader/core-loader.component'

import { FilterPipe } from './pipes/filter.pipe'
import { SortPipe } from './pipes/sort.pipe'

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    CoreTableComponent,
    CoreSelectSpinnerComponent,
    CoreLoaderComponent,
    FilterPipe,
    SortPipe
  ],
  exports: [
    CoreTableComponent,
    CoreSelectSpinnerComponent,
    CoreLoaderComponent
  ]
})
export class CoreModule {}
