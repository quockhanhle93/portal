import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from './shared.module'

import { OrganizationConfigurationComponent } from './components/organization-configuration/organization-configuration'
import { IpamProfileComponent } from './components/ipam-profile/ipam-profile.component'
import { CoreModule } from './core.module'

const routes: Routes = [
  { path: '', component: OrganizationConfigurationComponent },
  { path: ':action', component: IpamProfileComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    OrganizationConfigurationComponent,
    IpamProfileComponent
  ]
})
export class IpamConfigurationModule {}
