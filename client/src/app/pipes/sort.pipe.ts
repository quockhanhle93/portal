import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  transform(items: any[], index: number, reverse?: boolean): any {
    return items.sort((a, b) => {
      if (reverse) {
        if (a[index] < b[index]) {
          return -1
        } else if (a[index] > b[index]) {
          return 1
        } else {
          return 0
        }
      } else {
        if (b[index] < a[index]) {
          return -1
        } else if (b[index] > a[index]) {
          return 1
        } else {
          return 0
        }
      }
    })
  }
}
