import { Pipe, PipeTransform, Injectable } from '@angular/core'

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], value: string): any[] {
    if (!items) {
        return []
    }
    if (!value) {
        return items
    }
    return items.filter(item => {
      for (let i = 0; i < item.length; i++) {
        if (item[i].toString().toLowerCase().indexOf(value.toLowerCase()) > -1) {
          return true
        }
      }
      return false
    })
  }
}
