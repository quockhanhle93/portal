import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { CoreModule } from './core.module'
import { SharedModule } from './shared.module'

import { UserListComponent } from './components/user-list/user-list.component'
import { UserProfileComponent } from './components/user-profile/user-profile.component'

const routes: Routes = [
  { path: '', component: UserListComponent },
  { path: ':action', component: UserProfileComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
    CoreModule
  ],
  declarations: [
    UserListComponent,
    UserProfileComponent
  ]
})
export class UserConfigurationModule {}
