import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from './shared.module'
import { SelfServiceComponent } from './components/self-service/self-service.component'

const routes: Routes = [
  { path: '', component: SelfServiceComponent }
]

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    SelfServiceComponent
  ]
})
export class SelfServiceModule {}
