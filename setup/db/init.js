const f = require('util').format
const cryptoJS = require('crypto-js')
const secret = process.env.SECRET
const password = process.env.PCN_MASTER_PWD || 'pcnadmin'

const hasAuthen = process.env.PCN_DB_USER && process.env.PCN_DB_PWD
const uri = hasAuthen ?
    f('mongodb://%s:%s@%s', process.env.PCN_DB_USER, process.env.PCN_DB_PWD, process.env.PCN_DB_URL)
  : f('mongodb://%s', process.env.PCN_DB_URL)

const mongodb = require('mongodb').MongoClient
mongodb.connect(uri, (err, db) => {
  if(err){
    console.log(`Cannot connect to server ${uri}`)
    throw err
  }
  console.log(`Connected to server ${uri}`)

  initIpams(db)
  initOrgs(db)
  initGlobalConfig(db)
  initLanguages(db)
  initUsers(db)
  initSelfServices(db)
  initRoles(db)
  initRosettaStone(db)
  initRosettaStoneType(db)

  db.close()
  console.log('Done. Disconnected')
})

function readConfig(name){
  const fs = require('fs')
  const path = require('path')
  const data = JSON.parse(fs.readFileSync(path.join(__dirname, name), 'utf8'))
  return data
}

function initIpams(db) {
  const ipams = readConfig('supported-ipams.json').ipams

  db.collection('ipams').deleteMany()
  console.log('Deleted all ipam records')

  for (let idx = 0, len = ipams.length; idx < len; idx ++) {
    let ipam = ipams[idx]
    db.collection('ipams').insertOne({
      name: ipam.name,
      version: ipam.version,
      apiPath: ipam.apiPath,
    })
    console.log(`Inserted ${ipam.name} ${ipam.version} into database`)
  }
}

function initOrgs(db) {
  const orgs = readConfig('default_orgs.json').organizations

  db.collection('orgs').deleteMany()
  console.log('Deleted all organization records')

  for (let idx = 0, len = orgs.length; idx < len; idx ++) {
    let org = orgs[idx]
    db.collection('orgs').insertOne({name: org.name})
    console.log(`Inserted ${org.name} into database`)
  }
}

function initGlobalConfig(db) {
  const config = readConfig('default-global-config.json').config

  db.collection('globals').deleteMany()
  console.log('Deleted global-config record')

  db.collection('globals').insertOne(config)
  console.log(`Inserted global config into database`)
}


function initLanguages(db) {
  const languages = readConfig('supported-languages.json').languages

  db.collection('languages').deleteMany()
  console.log('Deleted all language records')

  for (let idx = 0, len = languages.length; idx < len; idx ++) {
    db.collection('languages').insertOne(languages[idx])
    console.log(`Inserted ${languages[idx]} into database`)
  }
}

function initUsers(db) {
  const users = readConfig('default_users.json').users

  db.collection('users').deleteMany()
  console.log('Deleted all user records')
  for (let idx = 0, len = users.length; idx < len; idx ++) {
    let user = users[idx]
    user.password = encrypt(password).toString()
    db.collection('users').insertOne(user)
    console.log(`Inserted ${user.username} into database`)
  }
}

function encrypt(plainText) {
    return cryptoJS.AES.encrypt(plainText, secret)
}

function initSelfServices(db) {
  const services = readConfig('access-service.json').accessServices

  db.collection('selfServices').deleteMany()
  console.log('Deleted all self service records')

  for (let idx = 0, len = services.length; idx < len; idx ++) {
    let service = services[idx]
    db.collection('selfServices').insertOne(service)
    console.log(`Inserted ${service.name} into database`)
  }
}

function initRoles(db) {
  const roles = readConfig('role.json').roles

  db.collection('roles').deleteMany()
  console.log('Deleted all role records')
  for (let idx = 0, len = roles.length; idx < len; idx ++) {
    let role = roles[idx]
    db.collection('roles').insertOne(role)
    console.log(`Inserted ${role.role} into database`)
  }
}

function initRosettaStone(db) {
  const rosettas = readConfig('rosetta-stone.json').rosettaStone

  db.collection('rosettaStones').deleteMany()
  console.log('Deleted all rosetta stone records')
  for (let idx = 0, len = rosettas.length; idx < len; idx ++) {
    let rosetta = rosettas[idx]
    db.collection('rosettaStones').insertOne(rosetta)
    console.log(`Inserted rosetta ${rosetta.actionId} into database`)
  }
}

function initRosettaStoneType(db) {
  const rosettaTypes = readConfig('rosetta-stone-type.json').rosettaStoneType

  db.collection('rosettaStoneTypes').deleteMany()
  console.log('Deleted all rosetta stone type records')
  for (let idx = 0, len = rosettaTypes.length; idx < len; idx ++) {
    let rosettaType = rosettaTypes[idx]
    db.collection('rosettaStoneTypes').insertOne(rosettaType)
    console.log(`Inserted rosetta ${rosettaType.actionId} - ${rosettaType.actionType} into database`)
  }
}

