# PCN Portal Server

> Server implementation for PCN Portal

## How to Start

``` bash
# install dependencies
npm install

# start server at localhost:8080 without debugging
npm start
```

Or use ``VS Code > Debug`` to start the server with debugging.
