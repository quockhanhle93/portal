const mongoose = require('mongoose')
const rosettaStoneTypeSchema = new mongoose.Schema({
  actionId: String,
  actionType: String,
  description: String,
  macd: Number,
})
module.exports = mongoose.model('rosettaStoneType', rosettaStoneTypeSchema, 'rosettaStoneTypes')
