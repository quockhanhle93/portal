const RosettaStone = require('./model')

exports.getAll = (req, res, next) => {
  RosettaStone.find().exec()
    .then((data) => success(200, data, req, next))
    .catch((err) => error(500, err, req, next))
}

exports.getOne = (req, res, next) => {
  RosettaStone.findById(req.params.id).exec()
    .then((data) => {
      if (data) {
        success(200, data, req, next)
      } else {
        error(404, 'Not found', req, next)
      }
    })
    .catch((err) => error(404, 'Not found', req, next))
}

const allowUpdated = ['description']

exports.put = (req, res, next) => {
  const data = req.body
  for (key in data) {
    if (allowUpdated.indexOf(key) === -1) {
      error(400, 'Invalid field(s)', req, next)
      return
    }
  }
  RosettaStone.findByIdAndUpdate(req.params.id, data).exec()
    .then((data) => {
      if (data) {
        success(200, {id: data._id}, req, next)
      } else {
        error(404, 'Not found', req, next)
      }
    })
    .catch((err) => error(404, 'Not found', req, next))
}

let success = (code, data, req, next) => {
  req.code = code
  req.data = data
  next()
}

let error = (code, err, req, next) => {
  req.code = code
  req.data = err
  next()
}
