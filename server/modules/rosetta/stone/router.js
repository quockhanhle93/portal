const express = require('express')
const r = new express.Router()
const handle = require('./handler')
const guard = require('../../acl').guard
const allow = guard.allow
const tag = 'rosetta'

r.get('/', allow(guard.readAny(tag)), handle.getAll)

r.get('/:id', allow(guard.readAny(tag)), handle.getOne)

r.put('/:id', allow(guard.updateAny(tag)), handle.put)

module.exports = r
