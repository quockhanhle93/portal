const mongoose = require('mongoose')
const rosettaStoneSchema = new mongoose.Schema({
  actionId: String,
  description: String,
})
module.exports = mongoose.model('rosettaStone', rosettaStoneSchema, 'rosettaStones')
