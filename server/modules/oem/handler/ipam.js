const Ipam = require('../ipam')
const selectFields = '-_version -apiPath'

module.exports.findAll = function() {
  return new Promise((resolve, reject) => {
    Ipam.find().select(selectFields).exec((err, ipams) => {
      if (err) {
        reject(err)
      } else {
        resolve(ipams)
      }
    })
  })
}

module.exports.findById = function(id) {
  return new Promise((resolve, reject) => {
    Ipam.findById(id).select(selectFields).exec((err, ipam) => {
      if (err) {
        reject(err)
      } else if (!ipam) {
        reject('Not found')
      } else {
        resolve(ipam)
      }
    })
  })
}

