const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ipamSchema = new Schema({
  _version: {
    type: Number,
    default: 1,
  },
  name: {
    type: String,
    unique: true,
    required: [true, 'cannot be empty'],
  },
  version: {
    type: String,
    unique: true,
    required: [true, 'cannot be empty'],
  },
  apiPath: {
    type: String,
    required: [true, 'cannot be empty'],
  },
}, {
  versionKey: false,
  timestamps: true,
})

const ipam = mongoose.model('ipam', ipamSchema, 'ipams')

module.exports = ipam
