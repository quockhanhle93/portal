const express = require('express')
const router = new express.Router()
const ipamHandler = require('./handler').ipam

router.get('/', (req, res, next) => {
  ipamHandler.findAll()
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

router.get('/:id', (req, res, next) => {
  ipamHandler.findById(req.params.id)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

module.exports = router
