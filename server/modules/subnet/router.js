const express = require('express')
const router = new express.Router()
const path = require('path')
const handlerDir = 'handler'

const Mapper = require('../mapper')
const HandlerMapper = new Mapper(
  path.join(path.join(__dirname, 'handler', 'z-conf.json')),
  handlerDir
)

// Mock data because the user and org module are not implemented yet
// In fact, we will get the IPAM information from organization module
const info = {
  ipam: 'bluecat',
  version: '8.1.1',
}

router.get('/', (req, res, next) => {
  require(HandlerMapper.handle('get-all', info)).send()
  next()
})

router.get('/:id', (req, res, next) => {
  res.send(require(HandlerMapper.handle('get', info)).send())
  next()
})

router.delete('/:id', (req, res, next) => {
  require(HandlerMapper.handle('delete', info)).send()
  next()
})

router.post('/', (req, res, next) => {
  require(HandlerMapper.handle('post', info)).send()
  next()
})

router.put('/:id', (req, res, next) => {
  require(HandlerMapper.handle('put', info)).send()
  next()
})


module.exports = router
