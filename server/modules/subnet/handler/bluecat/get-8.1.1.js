const logger = require('../../../logger')

exports.send = function() {
  logger.info('Getting subnet')
  return 'Network: 10.10.32.0/24'
}
