const mongo = require('mongoose')
const Schema = mongo.Schema

const globalSchema = new Schema({
  _id: {
    type: Number,
    default: 1,
  },
  _version: {
    type: Number,
    default: 1,
  },
  language: String,
}, {
  versionKey: false,
  timestamps: true,
})

const global = mongo.model('global', globalSchema, 'globals')

module.exports = global
