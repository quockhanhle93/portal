const mongo = require('mongoose')
const Schema = mongo.Schema

const languageSchema = new Schema({
  name: String,
  code: String,
}, {_id: false, versionKey: false})

const language = mongo.model('language', languageSchema, 'languages')

module.exports = language
