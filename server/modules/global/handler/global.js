const Global = require('../global')
const Language = require('../language')

const selectFields = '-_version -updatedAt -createdAt -_id'

module.exports.find = () => {
  return new Promise((resolve, reject) => {
    Global.find().select().exec((err, result) => {
      if (err) {
        reject(err)
      } else if (!result) {
        reject('Not found')
      } else {
        resolve(result)
      }
    })
  })
}

module.exports.findLanguage = () => {
  return new Promise((resolve, reject) => {
    Global.findOne({_id: 1}).select(selectFields).exec((err, result) => {
      if (err) {
        reject(err)
      } else if (!result) {
        reject('Not found')
      } else {
        resolve(result)
      }
    })
  })
}

module.exports.update = (data) => {
  return new Promise((resolve, reject) => {
    Global.update({_id: 1}, data, {upsert: true, setDefaultsOnInsert: true}, (err, result) => {
      if (err) {
        reject(err)
      } else if (!result) {
        reject('Not found')
      } else {
        resolve(data)
      }
    });
  })
}

module.exports.findLanguages = () => {
  return new Promise((resolve, reject) => {
    Language.find().select(selectFields).exec((err, result) => {
      if (err) {
        reject(err)
      } else if (!result) {
        reject('Not found')
      } else {
        resolve(result)
      }
    })
  })
}
