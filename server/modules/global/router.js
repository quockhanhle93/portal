const express = require('express')
const router = new express.Router()
const handler = require('./handler').global

// Get global configuration
router.get('/', (req, res, next) => {
  handler.find()
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Update global configuration and create one if does not exist
router.put('/', (req, res, next) => {
  handler.update(req.body)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// get supported languages
router.get('/languages', (req, res, next) => {
  let promise;
  if (req.param('selected') == 'true') {
    promise = handler.findLanguage()
  } else {
    promise = handler.findLanguages()
  }
  promise.then((data) => {
    req.data = data
    req.code = 200
    next()
  })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

module.exports = router
