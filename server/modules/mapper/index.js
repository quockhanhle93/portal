const fs = require('fs')
const path = require('path')

/**
 * This class is used to store logic for reading/caching mapping file configuration
 */
class Mapper {
  /**
   * @param {String} confPath ABSOLUTE path to configurations file
   * @param {String} handlerDir RELATIVE path to folder store handler files
   */
  constructor(confPath, handlerDir) {
    this.mappingConf = JSON.parse(fs.readFileSync(confPath, 'utf8'))
    this.handlerDir = handlerDir
  }

  /**
   *
   * @param {String} baseUrl Base Url
   * @param {JSON} info IPAM information [ipam, version]
   *
   * @return {String} handler file
   */
  handle(baseUrl, info) {
    const logicFile = this.mappingConf[info.ipam][info.version][baseUrl]
    return './' + path.join(this.handlerDir, info.ipam, logicFile)
  }
}

module.exports = Mapper
