const publicEndpoint = ['/v1/auth/login', '/v1/global/languages']

exports.first = (req, res, next) => {
  // Define attributes need for standard request/response
  req.data = null
  req.code = null

  req._rosettaSend = (category, actionId, actionType, data, code) => {
    req.rosetta_category = category
    req.rosetta_actionId = actionId
    req.rosetta_actionType = actionType
    req.rosetta_data = data
    req.rosetta_code = code

    req.code = req.rosetta_code
    req.data = req.rosetta_data
    return req
  }

  res.setHeader('Last-Modified', (new Date()).toUTCString())

  // Check if session is valid
  if (publicEndpoint.indexOf(req.path) == -1 && !req.isAuthenticated()) {
    res.clearCookie('authenticated')
    res.sendStatus(401)
    res.end()
  } else {
    next()
  }
}
exports.last = (req, res, next) => {
  if (!req.code) {
    req.code = 404
    req.data = {'error': `CANNOT ${req.method} API ${req.originalUrl}`}
  }
  res.status(req.code).send(req.data)
}

