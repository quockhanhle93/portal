const express = require('express')
const router = new express.Router()
const handler = require('./handler')

router.use(require('../audit').activity)
router.use(handler.first)

// Authentication and permission
router.use('/v1/auth', require('../authentication').router)
router.use(require('../acl').grant.setGrants)

// Self services
router.use('/v1/subnets', require('../subnet').router)
router.use('/v1/networks', require('../network').router)

// Other requests
router.use('/v1/users', require('../user').router)
router.use('/v1/ipams', require('../oem').router)
router.use('/v1/organizations', require('../org').router)
router.use('/v1/global', require('../global').router)
router.use('/v1/rosetta-stone', require('../rosetta').stoneRouter)
router.use('/v1/rosetta-stone-type', require('../rosetta').typeRouter)

router.use(handler.last)


module.exports = router

