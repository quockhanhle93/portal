const Org = require('../org')
const selectFields = '-_version -updatedAt -createdAt -ipams'

module.exports.findAll = function() {
  return new Promise((resolve, reject) => {
    Org.find().select(selectFields).exec((err, orgs) => {
      if (err) {
        reject(err)
      } else {
        resolve(orgs)
      }
    })
  })
}

module.exports.findById = function(id) {
  return new Promise((resolve, reject) => {
    Org.findById(id).select(selectFields).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve(org)
      }
    })
  })
}

module.exports.updateOne = function(id, data) {
  return new Promise((resolve, reject) => {
    Org.findByIdAndUpdate(id, data).select(selectFields).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve({id: org._id})
      }
    })
  })
}


