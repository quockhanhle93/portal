const Org = require('../org')

module.exports.findAll = function(orgId) {
  return new Promise((resolve, reject) => {
    Org.findById(orgId).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve(org.ipams)
      }
    })
  })
}

module.exports.findById = function(orgId, ipamId) {
  return new Promise((resolve, reject) => {
    Org.findOne(
      {'_id': orgId, 'ipams._id': ipamId},
      {ipams: {$elemMatch: {_id: ipamId}}}
    ).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve(org.ipams[0])
      }
    })
  })
}

module.exports.insertOne = function(orgId, data) {
  return new Promise((resolve, reject) => {
    Org.findByIdAndUpdate(
      orgId,
      {$addToSet: {ipams: data}},
      {'new': true}
    ).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve({id: org.ipams[org.ipams.length - 1]._id})
      }
    })
  })
}

module.exports.deleteOne = function(orgId, ipamId) {
  return new Promise((resolve, reject) => {
    Org.findOneAndUpdate(
      {'_id': orgId, 'ipams._id': ipamId},
      {$pull: {ipams: {_id: ipamId}}}
    ).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve({id: ipamId})
      }
    })
  })
}

module.exports.updateOne = function(orgId, ipamId, data) {
  return new Promise((resolve, reject) => {
    let updateObj = {}
    for (key in data) {
      if (key != '_id') {
        updateObj['ipams.$.' + key] = data[key]
      }
    }

    Org.findOneAndUpdate(
      {'_id': orgId, 'ipams._id': ipamId},
      {$set: updateObj}
    ).exec((err, org) => {
      if (err) {
        reject(err)
      } else if (!org) {
        reject('Not found')
      } else {
        resolve({id: ipamId})
      }
    })
  })
}
