const org = require('./org')
const ipam = require('./ipam')

module.exports = {
  org,
  ipam,
}
