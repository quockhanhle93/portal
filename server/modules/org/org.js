const mongo = require('mongoose')
const Schema = mongo.Schema

const ipSchema = new Schema({
  primary: {
    type: String,
    required: [true, 'cannot be empty'],
    sparse: true,
  },
  secondary: String,
  tertiary: String,
}, {_id: false})

const ipamSchema = new Schema({
  name: {
    type: String,
    required: [true, 'cannot be empty'],
    sparse: true,
  },
  version: {
    type: String,
    required: [true, 'cannot be empty'],
    sparse: true,
  },
  ipAddress: ipSchema,
  protocol: {
    type: String,
    default: 'http',
  },
  port: {
    type: Number,
    default: 80,
  },
  environment: String,
  dnsView: String,
  enabled: {
    type: Boolean,
    default: true,
  },
})

const orgSchema = new Schema({
  _version: {
    type: Number, default: 1,
  },
  name: {
    type: String,
    unique: true,
    required: [true, 'cannot be empty'],
  },
  ipams: [ipamSchema],
  language: String,
}, {
  versionKey: false,
  timestamps: true,
})

const org = mongo.model('org', orgSchema, 'orgs')

module.exports = org
