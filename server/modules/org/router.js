const express = require('express')
const router = new express.Router()
const orgHandler = require('./handler').org
const ipamHandler = require('./handler').ipam

// Get all organizations
router.get('/', (req, res, next) => {
  orgHandler.findAll()
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Get ogranization by id
router.get('/:id', (req, res, next) => {
  orgHandler.findById(req.params.id)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Update organization by id
router.put('/:id', (req, res, next) => {
  orgHandler.updateOne(req.params.id, req.body)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Get all ipams belong to organization
router.get('/:orgId/ipams', (req, res, next) => {
  ipamHandler.findAll(req.params.orgId)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Get ipam belongs to ogranization by id
router.get('/:orgId/ipams/:ipamId', (req, res, next) => {
  ipamHandler.findById(req.params.orgId, req.params.ipamId)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Update ipam belongs to organization by id
router.put('/:orgId/ipams/:ipamId', (req, res, next) => {
  ipamHandler.updateOne(req.params.orgId, req.params.ipamId, req.body)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Insert ipam to organization
router.post('/:orgId/ipams', (req, res, next) => {
  ipamHandler.insertOne(req.params.orgId, req.body)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

// Delete ipam belongs to organization by id
router.delete('/:orgId/ipams/:ipamId', (req, res, next) => {
  ipamHandler.deleteOne(req.params.orgId, req.params.ipamId)
    .then((data) => {
      req.data = data
      req.code = 200
      next()
    })
    .catch((err) => {
      req.data = err
      req.code = 404
      next()
    })
})

module.exports = router
