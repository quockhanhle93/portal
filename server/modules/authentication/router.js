const express = require('express')
const router = new express.Router()
const {passport} = require('./passport')

router.post('/login', function(req, res, next) {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      req.data = err
      return next(err)
    }
    if (!user) {
      req.code = 400;
      return next()
    }
    req.logIn(user, function(err) {
      if (err) {
        req.data = err
        return next(err)
      }
      res.cookie('authenticated', true)
      let cloneUser = Object.assign({}, user);
      delete cloneUser.password
      req.code = 200
      req.data = cloneUser
      next()
    });
  })(req, res, next);
});

router.get('/logout', function(req, res, next) {
  req.logout()
  req.code = 200
  req.data = {};
  next()
})

exports.router = router
