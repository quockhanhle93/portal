const userHandler = require('../user').user

/**
 * mock data for user
 * @param {String} username
 * @return {{username: string, password: string}}
 */
function loadUserByUsername(username) {
  return userHandler.findByUsernameWithPassword(username);
}

module.exports = {loadUserByUsername}
