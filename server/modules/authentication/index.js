const passport = require('./passport')
const router = require('./router')

module.exports = Object.assign({}, router, passport)
