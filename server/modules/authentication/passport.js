const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const crypto = require('../common/crypto')
const provider = require('./provider')

passport.serializeUser((user, cb) => {
  cb(null, user.username);
});

passport.deserializeUser((username, cb) => {
  provider.loadUserByUsername(username).then((user) => {
    if (!user) {
      return cb('error');
    }
    cb(null, user);
  }).catch((er) => {
    return cb('error');
  })
});

passport.use('local', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
},
(username, password, cb) => {
  provider.loadUserByUsername(username).then((data) => {
    let user = data._doc
    if (!user || !crypto.matches(password, user.password)) {
      return cb(null, false);
    }
    return cb(null, user);
  }).catch((err) => {
    return cb(null, false);
  })
})
)

exports.passport = passport
