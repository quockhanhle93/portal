const role = require('./role')

exports.setGrants = (req, res, next) => {
  if (req.user) {
    req.grants = [].concat(accessService(req))
    findUserGrants(req).then((grants) => {
      req.grants = req.grants.concat(accessUser(req, grants))
      next()
    })
  } else {
    next()
  }
}

let accessService = (req) => {
  let serviceGrants = []
  let serviceTags = req.user.accessServices
  if (serviceTags) {
    for (let idx = 0, len = serviceTags.length; idx < len; idx ++) {
      let map = new Map()
      map.role = req.user._id.toString()
      map.action = 'create:any'
      map.attributes = '*'
      map.resource = serviceTags[idx]
      serviceGrants.push(map)
    }
  }
  return serviceGrants
}

let findUserGrants = (req) => {
  return role.find({role: req.user.role}).select('action resource attributes').exec()
}

let accessUser = (req, grants) => {
  let userGrants = []
  if (grants) {
    for (let idx = 0, len = grants.length; idx < len; idx ++) {
      let grant = grants[idx]
      let map = new Map()
      map.role = req.user._id.toString()
      map.action = grant.action
      map.attributes = grant.attributes
      map.resource = grant.action.endsWith('own') ? req.user.username : grant.resource
      userGrants.push(map)
    }
  }
  return userGrants
}

