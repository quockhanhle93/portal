const mongo = require('mongoose')
const Schema = mongo.Schema

const roleSchema = new Schema({
  role: String,
  resource: String,
  action: String,
  attributes: String,
})

module.exports = mongo.model('role', roleSchema, 'roles')
