exports.readAny = function(resource) {
  return function(acl, who) {
    return acl.can(who).readAny(resource).granted
  }
}

exports.readOwn = function(resource, ownParameter) {
  return function(acl, who, req) {
    return acl.can(who).readAny(resource).granted ||
           acl.can(who).readOwn(req.params[ownParameter]).granted
  }
}

exports.createAny = function(resource) {
  return function(acl, who) {
    return acl.can(who).createAny(resource).granted
  }
}

exports.createOwn = function(resource, ownParameter) {
  return function(acl, who, req) {
    return acl.can(who).createAny(resource).granted ||
           acl.can(who).createOwn(req.params[ownParameter]).granted
  }
}

exports.deleteAny = function(resource) {
  return function(acl, who) {
    return acl.can(who).deleteAny(resource).granted
  }
}

exports.deleteOwn = function(resource, ownParameter) {
  return function(acl, who, req) {
    return acl.can(who).deleteAny(resource).granted ||
           acl.can(who).deleteOwn(req.params[ownParameter]).granted
  }
}

exports.updateAny = function(resource) {
  return function(acl, who) {
    return acl.can(who).updateAny(resource).granted
  }
}

exports.updateOwn = function(resource, ownParameter) {
  return function(acl, who, req) {
    return acl.can(who).updateAny(resource).granted ||
           acl.can(who).updateOwn(req.params[ownParameter]).granted
  }
}

exports.allow = function(action) {
  return (req, res, next) => {
    if (req.grants && req.grants.length > 0) {
      const userId = req.user._id.toString()
      const AccessControl = require('accesscontrol')
      const roleAcl = new AccessControl(req.grants)
      let access = action(roleAcl, userId, req)
      if (access) {
        return next()
      }
    }
    req.data = 'Permission denied'
    res.status(403).json('Permission denied')
  }
}
