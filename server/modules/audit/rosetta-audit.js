const mongoose = require('mongoose')
const rosettaAudit = new mongoose.Schema({
  requestId: String,
  user: String,
  category: String,
  actionId: String,
  actionType: String,
  details: Object,
  start: Date,
  end: Date,
  status: String,
  org: String,
  oem: Object,
}, {versionKey: false})
module.exports = mongoose.model('rosetta', rosettaAudit)
