const mongoose = require('mongoose')
const activityAudit = new mongoose.Schema({
  user: String,
  path: String,
  method: String,
  start: Date,
  end: Date,
  details: Object,
  status: String,
  org: String,
  oem: Object,
}, {versionKey: false})
module.exports = mongoose.model('activities', activityAudit)
