const meld = require('meld')
const Rosetta = require('./rosetta-audit')
const logger = require('../logger')

const afterSend = (start, req) => {
  return meld.after(req, '_rosettaSend', (req) => {
    const end = new Date()
    let auditData = {
      start: start,
      requestId: req.activityId,
      user: req.user && req.user.username ? req.user.username : '',
      category: req.rosetta_category,
      actionId: req.rosetta_actionId,
      actionType: req.rosetta_actionType,
      end: end,
      details: req.rosetta_data,
      status: req.rosetta_code,
    }
    const rosetta = new Rosetta(auditData)
    rosetta.save().catch((err) => logger.error(`Failed to track rosetta on ${data}`))
  })
}

const audit = (req, res, next) => {
  const start = new Date()
  afterSend(start, req)
  next()
}

module.exports = audit
