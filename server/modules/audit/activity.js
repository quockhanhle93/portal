const meld = require('meld')
const Activity = require('./activity-audit')
const logger = require('../logger')

const afterSend = (res) => {
  return meld.after(res, 'send', (result) => {
    const end = new Date()
    const req = result.req
    req.melder.remove()
    let auditData = {
      end: end,
      details: req.data,
      status: req.code || result.statusCode,
    }
    if (req.user && req.user.username) {
      auditData['user'] = req.user.username
    }
    Activity.findByIdAndUpdate(req.activityId, auditData).exec()
      .catch((err) => logger.error(`Failed to track activiy on ${auditData}`))
  })
}

const audit = (req, res, next) => {
  const start = new Date()
  const activity = new Activity({
    start: start,
    user: req.user && req.user.username ? req.user.username : '',
    path: req.originalUrl,
    method: req.method,
  })
  req.melder = afterSend(res)
  activity.save().then((data) => {
    req.activityId = data._id
    next()
  }).catch((err) => {
    logger.error(`Failed to track activiy on ${data}`)
    next()
  })
}

module.exports = audit
