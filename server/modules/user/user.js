const mongoose = require('mongoose')
const crypto = require('./crypto')

const locationSchema = new mongoose.Schema({
  address1: String,
  address2: String,
  city: String,
  state: String,
  country: String,
  postal: Number,
}, {timestamps: false, _id: false, versionKey: false})

const userSchema = new mongoose.Schema({
  _version: {
    type: Number,
    default: 1,
  },
  username: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, 'can\'t be blank'],
    match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
    index: true,
  },
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, 'can\'t be blank'],
    match: [/\S+@\S+\.\S+/, 'is invalid'],
    index: true,
  },
  location: locationSchema,
  password: String,
  firstName: String,
  lastName: String,
  image: String,
  enabled: {
    type: Boolean,
    default: true,
  },
  accessGroups: [Object],
  accessServices: [String],
  role: {
    type: String,
    lowercase: true,
    enum: ['user', 'normal', 'master'],
    default: 'normal',
  },
  language: String,
  timezone: String,
  orgs: [{
    name: String,
    _id: String,
    language: String,
  }],
}, {timestamps: true, versionKey: false})

userSchema.pre('save', function(next) {
  let user = this
  if (user.isModified('password')) {
    this.password = crypto.encrypt(this.password)
  }
  next()
})

module.exports = mongoose.model('user', userSchema, 'users')
