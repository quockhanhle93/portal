const express = require('express')
const r = new express.Router()
const handle = require('./handler/user')
const guard = require('../acl').guard
const allow = guard.allow

r.get('/', allow(guard.readAny('user')), handle.getAll)

r.get('/:username', allow(guard.readOwn('user', 'username')), handle.getOneByUsername)

r.post('/', allow(guard.createAny('user')), handle.post)

r.put('/:username', allow(guard.updateOwn('user', 'username')), handle.put)

r.put('/:username/password/', allow(guard.updateOwn('user', 'username')), handle.updatePass)

r.put('/:username/password/reset', allow(guard.updateOwn('user', 'username')), handle.resetPass)

r.delete('/:username', allow((acl, who, req) => {
  return req.user.username !== req.params['username'] && acl.can(who).deleteAny('user').granted
}), handle.delete)

r.put('/:username/status', allow(guard.updateOwn('user', 'username')), handle.updateStatus)

module.exports = r
