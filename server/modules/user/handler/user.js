const crypto = require('../crypto')
const User = require('../user')
const selectFields = '-_version -updatedAt -createdAt -password'

exports.getAll = (req, res, next) => {
  let query = req.query
  let page = query.page
  let limit = query.limit

  let filters = {}
  let supportedFilters = ['role', 'enabled', 'username', 'firstName', 'lastName']
  for (let idx = 0, len = supportedFilters.length; idx < len; idx++) {
    let filter = supportedFilters[idx]
    if (query[filter]) {
      filters[filter] = query[filter]
    }
  }
  if (query.org) {
    filters['orgs.name'] = query.org
  }

  User.find(filters).select(selectFields).limit(limit)
    .skip((page -1) * limit).exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else {
        User.count(filters).then((count) => {
          send(200, {
            result: result,
            total: count,
          }, req, next)
        })
      }
    })
}

exports.post = (req, res, next) => {
  let user = new User(req.body)
  return user.save()
    .then((data) => {
      send(201, {username: data.username}, req, next)
    })
    .catch((err) => {
      let errCode = errorMapping[err.code] || 500
      send(errCode, err, req, next)
    })
}

exports.put = (req, res, next) => {
  let data = req.body
  delete data.password
  User.findOneAndUpdate({username: req.params.username}, data)
    .select(selectFields).exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else {
        send(200, {username: result.username}, req, next)
      }
    })
}

exports.delete = (req, res, next) => {
  User.findOneAndRemove({username: req.params.username})
    .select(selectFields).exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else {
        send(200, {username: result.username}, req, next)
      }
    })
}

exports.getOneByUsername = (req, res, next) => {
  User.findOne({username: req.params.username}).select(selectFields)
    .exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else {
        send(200, result, req, next)
      }
    })
}

exports.resetPass = (req, res, next) => {
  let password = req.body.password
  User.findOne({username: req.params.username}, password)
    .select('username password').exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else {
        result.password = password
        result.save()
          .then((data) => {
            send(201, {username: data.username}, req, next)
          })
          .catch((err) => {
            let errCode = errorMapping[err.code]
            send(errCode, err, req, next)
          })
      }
    })
}

exports.updatePass = (req, res, next) => {
  let data = req.body
  User.findOne({username: username}, data)
    .select('username password').exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else if (!crypto.matches(data.oldPassword, result.password)) {
        send(400, {errmsg: 'Password unmatched'}, req, next)
      } else {
        result.password = data.newPassword
        result.save()
          .then((data) => {
            send(201, {username: data.username}, req, next)
          })
          .catch((err) => {
            let errCode = errorMapping[err.code]
            send(errCode, err, req, next)
          })
      }
    })
}

exports.updateStatus = (req, res, next) => {
  let enabled = req.params.enabled || req.body.enabled
  let username = req.params.username
  User.findOneAndUpdate({username: username}, {enabled: enabled})
    .select(selectFields).exec((err, result) => {
      if (err) {
        let errCode = errorMapping[err.code] || 500
        send(errCode, err, req, next)
      } else if (!result) {
        send(404, {errmsg: 'Not found'}, req, next)
      } else {
        send(201, {username: result.username}, req, next)
      }
    })
}

exports.findByUsernameWithPassword = (username) => {
  return new Promise((resolve, reject) => {
    User.findOne({username: username})
      .exec((err, result) => {
        if (err) {
          reject(err)
        } else if (!result) {
          reject('Not found')
        } else {
          resolve(result)
        }
      })
  })
}

let errorMapping = {
  11000: 409,
}

let send = (code, data, req, next) => {
  req.code = code
  req.data = data
  next()
}
