exports.tag = {
  network: {
    add: 'add_network',
    modify: 'add_network',
    getList: 'get_network_list',
  },
  block: {
    add: 'add_block',
  },
  ipDns: {
    addIpHostRecord: 'add_ipv4_address_host_record',
    deleteIpHost: 'delete_ipv4_address_host',
    addDns: 'add_dns_record',
    modifyDns: 'modify_dns_record',
    moveDns: 'move_dns_record_to_new_ip',
  },
  dhcp: {
    addScope: 'add_dhcp_scope',
    expand: 'expand_dhcp_scope_size',
    addReservation: 'add_dhcp_reservation',
  },
  ukRetail: {
    getStore: 'get_store_list',
    buildWincor: 'iss_build_wincor',
    cudStore: 'create_modify_delete_stone',
  },
}
