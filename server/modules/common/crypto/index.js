const cryptoJS = require('crypto-js')
const config = require('../../../conf/index')
/**
 * check whether a plain text matches the encrypted text
 *
 * @param {String} plainText the plain text
 * @param {String} encryptText the encrypt text
 * @return {boolean}
 */
function matches(plainText, encryptText) {
  let bytes = cryptoJS.AES.decrypt(encryptText.toString(), config.server.secret)
  return bytes.toString(cryptoJS.enc.Utf8) === plainText;
}

/**
 * encrypt text
 *
 * @param {String} plainText
 * @return {*|CipherParams}
 */
function encrypt(plainText) {
  return cryptoJS.AES.encrypt(plainText, config.server.secret)
}

module.exports = {matches, encrypt}
