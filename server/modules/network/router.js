const express = require('express')
const router = new express.Router()
const path = require('path')
const handlerDir = 'handler'
const Mapper = require('../mapper')
const guard = require('../acl').guard
const allow = guard.allow
const tag = require('../common/access-service').tag

const HandlerMapper = new Mapper(
  path.join(path.join(__dirname, 'handler', 'z-conf.json')),
  handlerDir
)

// Mock data because the user and org module are not implemented yet
// In fact, we will get the IPAM information from organization module
const info = {
  ipam: 'bluecat',
  version: '8.1.1',
}

router.use(require('../audit').rosetta)

router.post('/add',
  allow(guard.createAny(tag.network.add)),
  require(HandlerMapper.handle('add', info)))

module.exports = router
