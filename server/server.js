global.SERVER_DIR = __dirname

const express = require('express')
const app = express();
const path = require('path')

const i18n = require('i18n')

const config = require('./conf')
const logger = require('./modules/logger')

const gateway = require('./modules/gateway')
const parser = require('body-parser')
const cookieParser = require('cookie-parser')

const session = require('express-session')
const {passport} = require('./modules/authentication')
const mongoose = require('mongoose')
const MongoStore = require('connect-mongo')(session);
const paginate = require('express-paginate')

require('./modules/database')

app.use(parser.json())
app.use(parser.urlencoded({extended: false}))
app.use(cookieParser())

i18n.configure({
  locales: ['en'],
  defaultLocale: 'en',
  directory: __dirname + '/locales',
  cookie: 'lang',
});

app.use(i18n.init);
app.use(express.static(path.join(__dirname, config.statics.dir)))

app.use(paginate.middleware(10, 50))
app.use(session({
  name: 'psession',
  secret: config.server.session.secret,
  resave: false,
  saveUninitialized: false,
  store: new MongoStore({mongooseConnection: mongoose.connection}),
}))

app.use(passport.initialize())
app.use(passport.session())

app.all(function(req, res, next) {
  // set default or minimum is 10 (as it was prior to v0.2.0)
  if (req.query.limit <= 10) req.query.limit = 10
  next()
})
app.use('/api', gateway.web)
app.get('*', (req, res) => res.sendFile(path.join(__dirname,
  config.statics.dir, config.statics.index)))
const port = config.server.port

app.listen(port, (err) => {
  if (err) {
    logger.error(`Cannot start server on port ${port}`, err)
  } else {
    logger.info(`Listening on port ${port}`)
  }
})
