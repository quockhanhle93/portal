const acceptedLevel = ['error', 'warn', 'info', 'verbose', 'debug', 'silly']
const defaultLevel = process.env.NODE_ENV === 'development' ? 'debug' : 'info'

module.exports = {
  logger: {
    enabled: process.env.LOG_ENABLED !== 'false',
    level: acceptedLevel.indexOf(process.env.LOG_LEVEL) != -1 ?
      process.env.LOG_LEVEL : defaultLevel,
    logDir: 'logs',
    maxsize: (process.env.LOG_MAX_SIZE || 5) * 1048576, // 5MB,
    maxDays: (process.env.LOG_MAX_DAYS) || 365,
  },
}

