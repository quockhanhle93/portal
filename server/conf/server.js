module.exports = {
  server: {
    port: process.env.PORT || 80,
    session: {
      secret: process.env.SESSION_SECRET || 'PCN Secret', // use to encrypt session cookie
    },
    secret: process.env.SECRET || 'This1s4Rand0m', // use to encrypt user password
  },
}


