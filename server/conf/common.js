const staticsDir = process.env.STATICS_DIR || 'public'
const indexFile = process.env.INDEX_FILE || 'index.html'

module.exports = {
  env: process.env.NODE_ENV,
  statics: {
    dir: staticsDir,
    index: indexFile,
  },
}

