
const common = require('./common')
const server = require('./server')
const logger = require('./logger')
const db = require('./db')

module.exports = Object.assign({}, common, server, logger, db)
