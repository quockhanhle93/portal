const f = require('util').format

const hasAuthen = process.env.PCN_DB_USER && process.env.PCN_DB_PWD
const uri = hasAuthen ?
  f('mongodb://%s:%s@%s', process.env.PCN_DB_USER, process.env.PCN_DB_PWD, process.env.PCN_DB_URL)
  : f('mongodb://%s', process.env.PCN_DB_URL)

module.exports = {
  db: {
    uri: uri,
    poolSize: process.env.PCN_DB_POOL_SIZE || 4,
  },
}
