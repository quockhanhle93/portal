FROM dev01-ci01.pcn.lab:5000/node:latest

# Create portal directory
RUN mkdir -p /usr/src/portal
WORKDIR /usr/src/portal
# Copy portal distribution packages
COPY dist /usr/src/portal

ENV NODE_ENV=production
ENV PORT=80
ENV SESSION_SECRET="PCN Secret"

ENV LOG_LEVEL=debug
ENV LOG_MAX_SIZE=5
ENV LOG_MAX_DAYS=30

ENV PCN_DB_USER=pcnadmin
ENV PCN_DB_PWD=pcnadmin
ENV PCN_DB_POOL_SIZE=10
ENV PCN_DB_URL=192.168.88.175,192.168.88.176,192.168.88.151:27017/pcnportal?replicaSet=rs0
ENV SECRET="This1s4Rand0m"

WORKDIR /usr/src/portal
EXPOSE 80
CMD [ "npm", "start" ]
