// CONFIGURATIONS
const distDir = 'dist';
const clientDir = 'client';
const serverDir = 'server';
const setupDir = 'setup';
// DEPENDENCIES
const gulp = require('gulp');
const path = require('path');
const fs = require('fs-extra');
const shell = require('gulp-shell');
const runSequence = require('run-sequence');
const config = require('./server/conf');

// ALWAYS EXECUTED TASKS
console.log('PCN Portal BUILD');

// TASKS
gulp.task('default', function () {
  console.log('\n===========Available tasks================\n');
  console.log('build\n\tBuild PCN portal application (server and client intergration)');
  console.log('clean\n\tClean the built project');
  console.log('start\n\tStart the server project in production mode');
  console.log('\n==========================================\n');
});

gulp.task('installClient', function () {
  return gulp.src('').pipe(shell('npm install', { cwd: clientDir }));
});

gulp.task('installServer', function () {
  return gulp.src('').pipe(shell('npm install', { cwd: serverDir }));
});

gulp.task('installSetup', function () {
  return gulp.src('').pipe(shell('npm install', { cwd: setupDir }));
});

gulp.task('buildClient', function () {
  return gulp.src('').pipe(shell('npm run build', { cwd: clientDir }));
});

gulp.task('lintServer', function() {
  return gulp.src('').pipe(shell('npm run lint', { cwd: serverDir }));
});

gulp.task('lintClient', function() {
  return gulp.src('').pipe(shell('npm run lint', { cwd: clientDir }));
});

gulp.task('copyServer', function () {
  return gulp.src(path.join(serverDir, '**')).pipe(gulp.dest(path.join(distDir)));
})

gulp.task('copyClient', function() {
  return gulp.src(path.join(clientDir, distDir, '**')).pipe(gulp.dest(path.join(distDir, config.statics.dir)));
})

gulp.task('copySetup', function () {
  return gulp.src(path.join(setupDir, '**'), {base:"."}).pipe(gulp.dest(path.join(distDir)));
})

gulp.task('clean', function () {
  fs.removeSync(distDir);
  return gulp.src('').pipe(shell('npm run clean', { cwd: clientDir }));
});

gulp.task('build', function () {
  return runSequence('installServer', 'lintServer', 'installClient', 'lintClient',
  'installSetup', 'clean', 'buildClient', 'copyServer', 'copyClient', 'copySetup');
})