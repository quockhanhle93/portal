# PCN Portal

> This repository contains 2 projects corresponding to client and server implementations for PCN Portal.

## How to Start

### Backend Developement

``` bash
# change working directory to "server" project
cd server

# install dependencies
npm install

# start server at localhost:8080 without debugging
npm start
```

Or use ``Visual Studio Code`` IDE and the built-in ``Debug`` functionality to start the server with debugging.

Read ``README.md`` file to know more.

### Frontend Development

``` bash
# change working directory to "client"
cd client

# install dependencies
npm install

# start client app at localhost:9000
npm start
```

Read the local ``README.md`` file to know more about how to work with the project.

*Note: Client app requires Server app running to make it working correctly.*

## How to Build/Package Final Product

Change working directory to root of this project.

``` bash

# install dependencies
npm install

# build project: "dist" directory will be generated
npm run build

# start production server at localhost:8080
npm run start

# get more help
npm run help
````
